package hw15.service;

import hw15.Client;
import hw15.dao.ClientDao;
import hw15.dao.Dao;

import java.util.List;
import java.util.Optional;

public class ClientService {
    private static Dao<Client, Integer> clientDao;

    public ClientService() {
        clientDao = new ClientDao();
    }

    public void save(Client client) {
        clientDao.openSession();
        clientDao.save(client);
        clientDao.closeSession();
    }

    public void update(Client client) {
        clientDao.openSession();
        clientDao.update(client);
        clientDao.closeSession();
    }

    public void delete(Client client) {
        clientDao.openSession();
        clientDao.delete(client);
        clientDao.closeSession();
    }

    public Client findById(Integer id) {
        clientDao.openSession();
        Optional<Client> client = clientDao.findById(id);
        clientDao.closeSession();
        return client.get();
    }

    public List<Client> findAll() {
        clientDao.openSession();
        List clients = clientDao.findAll();
        clientDao.closeSession();
        return clients;
    }
}
