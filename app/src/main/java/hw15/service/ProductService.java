package hw15.service;

import hw15.Product;
import hw15.dao.Dao;
import hw15.dao.ProductDao;

import java.util.List;
import java.util.Optional;

public class ProductService {
    private static Dao<Product, Integer> productDao;

    public ProductService() {
        productDao = new ProductDao();
    }

    public void save(Product client) {
        productDao.openSession();
        productDao.save(client);
        productDao.closeSession();
    }

    public void update(Product client) {
        productDao.openSession();
        productDao.update(client);
        productDao.closeSession();
    }

    public void delete(Product client) {
        productDao.openSession();
        productDao.delete(client);
        productDao.closeSession();
    }

    public Product findById(Integer id) {
        productDao.openSession();
        Optional<Product> client = productDao.findById(id);
        productDao.closeSession();
        return client.get();
    }

    public List<Product> findAll() {
        productDao.openSession();
        List clients = productDao.findAll();
        productDao.closeSession();
        return clients;
    }
}
