package hw15.dao;

import hw15.Order;

import java.util.List;
import java.util.Optional;

public class OrderDao extends BaseDao<Order, Integer> {
    @Override
    public void save(Order order) {
        getSession().save(order);
    }

    @Override
    public void update(Order order) {
        getSession().update(order);
    }

    @Override
    public void delete(Order order) {
        getSession().delete(order);
    }

    @Override
    public Optional<Order> findById(Integer integer) {
        Order order = getSession().get(Order.class, integer);
        return (order == null) ? Optional.empty() : Optional.of(order);
    }

    @Override
    public List<Order> findAll() {
        return getSession().createQuery("from Order ").list();
    }
}
