package hw15.dao;

import org.hibernate.Session;

import java.util.List;
import java.util.Optional;

public interface Dao<Cls, Id> {
    void save(Cls cls);
    void update(Cls cls);
    void delete(Cls cls);
    Optional<Cls> findById(Id id);
    List<Cls> findAll();

    Session openSession();
    void closeSession();
}