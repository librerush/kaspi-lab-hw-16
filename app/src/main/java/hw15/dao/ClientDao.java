package hw15.dao;

import hw15.Client;

import java.util.List;
import java.util.Optional;

public class ClientDao extends BaseDao<Client, Integer> {

    @Override
    public void save(Client client) {
        getSession().save(client);
    }

    @Override
    public void update(Client client) {
        getSession().update(client);
    }

    @Override
    public void delete(Client client) {
        getSession().delete(client);
    }

    @Override
    public Optional<Client> findById(Integer integer) {
        Client client = getSession().get(Client.class, integer);
        if (client != null) {
            return Optional.of(client);
        }
        return Optional.empty();
    }

    @Override
    public List<Client> findAll() {
        return getSession().createQuery("from Client").list();
    }
}
