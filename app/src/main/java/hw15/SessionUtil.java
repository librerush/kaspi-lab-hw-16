package hw15;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import java.io.File;

public class SessionUtil {
    private static final SessionFactory sessionFactory = create();

    private static SessionFactory create() {
        try {
            return new Configuration().configure(new File("hibernate.cfg.xml")).buildSessionFactory();
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("SessionFactory failed..");
        }
    }

    public static SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public static void close() {
        sessionFactory.close();
    }
}
