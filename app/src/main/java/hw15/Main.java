package hw15;

import hw15.service.OrderService;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;


public class Main {
    static List<Product> popularProducts(int limit) {
        Map<Product, Integer> hashMap = new TreeMap<>((o1, o2) -> o1.hashCode() - o2.hashCode());
        List<Order> orders = new OrderService().findAll();
        for (Order order : orders) {
            List<Product> products = order.getProducts();
            for (Product product : products) {
                if (hashMap.containsKey(product)) {
                    Integer i = hashMap.get(product);
                    hashMap.put(product, i + 1);
                } else {
                    hashMap.put(product, 1);
                }
            }
        }

        List<Product> result = new ArrayList<>();

        AtomicInteger j = new AtomicInteger();
        hashMap.forEach((p, i) -> {
            if (j.get() < limit) {
                result.add(p);
                j.getAndIncrement();
            }
        });

        return result;
    }

    public static void main(String[] args) {
        try {
            System.out.println(popularProducts(5));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
