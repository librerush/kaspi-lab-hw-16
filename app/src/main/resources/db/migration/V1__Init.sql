CREATE TABLE client(
    id int primary key,
    name varchar(100),
    address varchar(100)
);

CREATE TABLE manufacturer(
    id int primary key,
    name varchar(100),
    site_link varchar(100)
);

CREATE TABLE "order"(
    id int primary key,
    client_id int references client(id),
    order_time date
);

CREATE TABLE product(
    id int primary key,
    name varchar(100),
    price float,
    manufacturer_id int references manufacturer(id)
);

CREATE TABLE order_product(
    order_id int references "order"(id),
    product_id int references product(id),
    product_quantity int
);
